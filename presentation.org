#+Title: Hands on Javascript Debugging
#+Author: Karel Kremer
* Git Repository
#+BEGIN_SRC bash
git clone git@bitbucket.org:iBizz/javascript-debug.git
#+END_SRC

* install dependencies
#+BEGIN_SRC bash
cd client 
npm i
cd ../server
npm i
#+END_SRC

* start program
Start server (port: 3001):
#+BEGIN_SRC bash
cd server
npm start
#+END_SRC

Start client (auto refresh!):
#+BEGIN_SRC bash
./node_modules/@angular/cli/bin/ng serve --proxy-config proxy.conf.json
#+END_SRC
* navigate to frontend
url: http://localhost:4200/5af997289a688d00054a78f5
* debugger interface - take aways
- elements: the dom tree
- network: the requests
- console
- search the sources
- profiler
- cache
* debugger - deep dive
- mainly source tab
- breakpoints
- call stack
- power of the console
- break on exceptions
- DOM listeners
* fix the issues!
- not all messages are visible
- empty message before quick replies
- not sending quick replies
* debugging the backend
#+BEGIN_SRC bash
nodemon --inspect-brk .
#+END_SRC

url: chrome://inspect
