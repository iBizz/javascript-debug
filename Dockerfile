FROM node:8.1.3

ARG ADMIN_URL=local
ARG CHATBOT_URL=local
ARG PORT=local
ENV PORT ${PORT}
ENV ADMIN_URL ${ADMIN_URL}
ENV CHATBOT_URL ${CHATBOT_URL}

WORKDIR /app
EXPOSE 443
ADD server/package.json /app/
RUN mkdir /app/client
ADD client/package.json /app/client/
RUN npm install
RUN cd client && npm install
ADD server/. /app/
ADD client/. /app/client/
RUN cd client && node_modules/@angular/cli/bin/ng build --env=prod && mv dist ../public
CMD node ./bin/www
