const express = require("express");
const path = require("path");
const favicon = require("serve-favicon");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
var https = require("https");
var sslConfig = require("./ssl-config");

const app = express();
app.set("port", 443);

const options = {
  key: sslConfig.privateKey,
  cert: sslConfig.certificate,
  ca: sslConfig.ca
};
const server = https.createServer(options, app);

const request = require("request-promise");
const oswald = require("./oswald.config");
let pattern = "";
const io = require("socket.io")(server);

// view engine setup
app.use(express.static(__dirname + "/public"));
// fallback for subroutes

app.use((req, res, next) => {
  if (req.get("Accept").indexOf("text/html") >= 0) {
    res.sendFile(`${__dirname}/public/index.html`);
  } else {
    next();
  }
});

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message || err);
});

const sendMessage = message => {
  const options = {
    method: "post",
    headers: {
      "content-type": "application/json",
      Authorization: oswald.token
    },
    json: true,
    uri: oswald.baseUrl + "/api/v1/chats/" + message.chatbot + "/message"
  };
  message.environment = "production";
  delete message.chatbot;
  options.body = message;
  return request(options).then(response => {
    io.emit(response.session, response);
  }, console.error);
};

io.on("connection", socket => {
  console.info("connected");

  socket.on("subscribe", all => {
    console.info("subscribed");
    io.emit("config", oswald);
  });

  socket.on("message", sendMessage);
});

module.exports = { app: app, server: server };
