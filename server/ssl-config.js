var path = require('path');
var fs = require('fs');

exports.privateKey = fs.readFileSync(path.join(__dirname, './ssl/privatekey.pem')).toString();
exports.certificate = fs.readFileSync(path.join(__dirname, './ssl/wildcard_oswald_ai.pem')).toString();
exports.ca = [
  fs.readFileSync(path.join(__dirname, './ssl/AddTrustExternalCARoot.crt')).toString(),
  fs.readFileSync(path.join(__dirname, './ssl/COMODORSAAddTrustCA.crt')).toString(),
  fs.readFileSync(path.join(__dirname, './ssl/COMODORSADomainValidationSecureServerCA.crt')).toString()
];
