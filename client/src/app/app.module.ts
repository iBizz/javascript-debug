import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MessagesComponent } from './message/message.component';
import { ChatService } from './service/chat.service';
import { JRQueue } from './service/queue';
import { HomeComponent } from './components/home/home.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import {MarkdownToHtmlModule} from "markdown-to-html-pipe";

const appRoutes: Routes = [
  {
    path: ':id',
    component: HomeComponent
  },
  { path: '**', component: NotFoundPageComponent }
];


@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule, MarkdownToHtmlModule,  RouterModule.forRoot(
    appRoutes
  )],
  declarations: [ AppComponent, MessagesComponent, HomeComponent, NotFoundPageComponent ],
  bootstrap:    [ AppComponent],
  providers:    [ ChatService, JRQueue ]
})

export class AppModule { }
