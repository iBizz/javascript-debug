import { Component, Renderer, HostListener, ElementRef, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { List } from 'immutable';

import { ChatService } from './../../service/chat.service';
import { JRQueue } from './../../service/queue';
import { OswaldResponse, Message, ChatMessage, OswaldMessage } from './../../models';

import { MessagesComponent } from './../../message/message.component';

@Component({
  selector    : 'app',
  templateUrl : './home.component.html',
  styleUrls   :  ['./home.component.scss']
})

export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('myInput') input: ElementRef;

  speakerChecked: String = 'disabled';
  message: String = '';
  time: string;
  currentMessage: any;
  speakEnabled: Boolean;
  askedForFeedback: Boolean = false;
  personSpoke: Boolean = true;
  textboxDisabled: Boolean = false;
  speakerEnabledShow: Boolean = false;
  conversation: BehaviorSubject<List<ChatMessage>>;
  state: any = { placeholder: 'Typ hier je bericht', textboxDisabled: false };
  receivedListSize: number = 0;
  lastAction: Date = null;
  name: String = 'Chatbot';
  id: String;
  private sub: any;
  background: String = 'images/pattern.png';

  constructor(public chatService: ChatService,
    private renderer: Renderer,
    public funnel: JRQueue,
    private route: ActivatedRoute,
    private http: Http) {}

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id) {
        this.chatService.setInit(this.id);
      }
   });

    const now = new Date();

    // set client properties
    this.time = now.getHours() + ':' + ('0' + now.getMinutes()).substr(-2)

    // bind observables from the service
    this.conversation = this.chatService.messages();
    this.chatService.state().subscribe(newState => this.setState(newState));

    // listen to the conversation
    this.conversation.subscribe(list => {
      this.onMessage(list);
    });
  }

  noObject(val) {
    return typeof val === 'string';
  }

  onMessage(list: List<ChatMessage>) {
    if (list.size > 0 && list.last().send) { return; }

    const added = list
      // only interested in Oswald's messages
      .filter(item => !item.send)

      // take the added part of the list since last time
      .slice(this.receivedListSize)

      // shallow convert to regular array
      .toArray();

    if (added.length === 0) { return; }

    // create a task for every new message
    const tasks = added
      .map(message => ({ callback: () => {
          this.currentMessage = message;
          setTimeout(function(){
            if(this.currentMessage){
              this.currentMessage.visible = true;
            }
          }, 200);
      } }));

      // send them through the funnel
    this.funnel.add(...tasks);

    // update received list size
    this.receivedListSize = this.receivedListSize + added.length;
  }

  sendText() {
    this.message = this.message.trim();
    if (!this.message) { return; }

    // TODO: proper way of preparing message before sending
    // this.message = this.message.replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    this.sendMessage({ message: this.message, type: 'text', chatbot: this.id });
    this.message = '';
  }

  isPastMessage(list, messageIndex) {
    return !(list.size <= messageIndex)
  }

  // special response, mostly radio input
  // id of the chosen answer in message and as payload (data)
  sendAction(answer: any, parent: ChatMessage, messageIndex: number, type: any = null) {

    const list = this.conversation.getValue();

    if (this.isPastMessage(list, messageIndex)) {
      return;
    }

    const message: OswaldMessage = parent.message;

    // remove payload
    delete message.elements;

    // send answer
    this.sendMessage({ message: answer.action, text: answer.label, chatbot: this.id, type: 'text' });

  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHander(event) {
    this.chatService.sendEvent({type: 'unloaded'});
  }

  didAction() {
    this.lastAction = new Date();
  }

  private sendMessage(msgObj) {
    this.didAction();
    this.chatService.send(msgObj);
    this.funnel.hold();
  }

  setState(newState: any = {}) {
    Object.assign(this.state, newState);
    this.state.placeholder = this.state.placeholder || '';
  }

  ngAfterViewInit() {
    this.input.nativeElement.focus();
  }
}
