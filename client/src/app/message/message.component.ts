import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'messages',
  templateUrl: './message.component.html',
  styleUrls:  ['./message.component.scss']
})

export class MessagesComponent  {
  @Input() data: any;
  @Output() radioClicked = new EventEmitter<any>();
  @Output() checkboxClicked = new EventEmitter<any>();

  constructor() { }

  noObject(val) {
    return typeof val === 'string';
  }

  clickRadio(message: string, id: string, type: string, clickable: boolean = true) {
    if (clickable !== false) {
      this.radioClicked.emit({ message, id, type, clickable });
    }
  }
}
