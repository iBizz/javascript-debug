import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';

import { OswaldResponse, Message, EmitMessage, ChatMessage } from '../models';

@Injectable()
export class JRQueue {

    private delay       : number;
    private timer       : any = null;
    private queue       : Array<{context: any, callback: any}> = [];

    public onChange$   : BehaviorSubject<number> = new BehaviorSubject(0);

    constructor() {
        this.delay = 800;
    }

    add(...task): void {
        this.queue.push(...task);
        this.onChange$.next(this.queue.length);

        // if no tasks are waiting, run immediately
        this.next();
    }

    // if no tasks are pending this makes sure the delay is respected before the next task is executed
    hold() {
        this.trigger();
    }

    private trigger(): number {
        // already task waiting
        if (this.timer) return;

        // delay between tasks
        this.timer = setTimeout(() => {
            // cleanup so task can be fulfilled
            this.clear();
            this.next();
        }, this.delay);

        return this.delay;
    }

    private next(): void {
        if (this.timer) return;

        const task = this.queue.shift();
        if (!task) {
            return;
        }
        this.onChange$.next(this.queue.length);

        task.callback.call(task.context);
        this.trigger();
    }

    private clear(): void {
        clearTimeout(this.timer);
        this.timer = null;
    }

}
