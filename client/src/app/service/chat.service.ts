import { UUID } from 'angular2-uuid';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/Rx';
import { List } from 'immutable';
import * as io from 'socket.io-client';
import { ActivatedRoute } from '@angular/router';

import { OswaldResponse, Message, EmitMessage, EmitEvent, ChatMessage } from '../models';

@Injectable()
export class ChatService {
    private _messages: BehaviorSubject<List<ChatMessage>>= new BehaviorSubject(List([]));
    private _state: BehaviorSubject<OswaldResponse>   = new BehaviorSubject(new OswaldResponse());

    private _socket: SocketIOClient.Socket             = io('/');
    private _id: string                            = UUID.UUID();

    constructor() {
        // bind events of the socket
    }

    public getId() {
        return this._id;
    }

    public setInit(id) {
      this._socket.on(this._id, (response: OswaldResponse) => this.receive(response));
      this._socket.on('config', config => this._state.next(config));
      this._socket.emit('subscribe');
      this._socket.send({ session: this._id, message: 'STARTCOMMANDO', chatbot: id});
    }

    public send(SendObj: EmitMessage) {
        SendObj.session = this._id;

        // send message to Oswald
        this._socket.emit('message', SendObj);

        // after sending, we only need the text
        delete SendObj.data;

        // Send Action but show Label
        if (SendObj.text) {
          SendObj.message = SendObj.text;
          delete SendObj.text;
        }

        // notify all subscribers
        this._messages.next(this._messages.getValue().push({ message: SendObj, time: new Date(), send: true, visible: true }));

        // clear state
        this._state.next(new OswaldResponse());
    }

    public sendEvent(SendObj: EmitEvent ) {
        SendObj.session = this._id;

        // send message to Oswald
        this._socket.emit('event', SendObj);
    }

    public receive(response?: any) {
        let messages = [];

        if (!response) { return } ;

        if (!response.data) {
          if (!response.message || !response.type) { return; };
          messages.push({ message: {message: response.message, type: 'welcome'}, time: new Date(), send: false, visible: false });
        }else {
          if (response.data.type === 'text' && !!!response.data.message) { return };
          messages = response.data.map(message => ({ message: message, time: new Date(), send: false, visible: false }));
        }

        if(response.quickReplies){
          messages.push({
            message: {
              type: "quickreply",
              message: { options: response.quickReplies.map((item) => {
                return {
                  label: item.text,
                  action: item.action
                }
              })}
            },
            time: new Date(),
            send: false,
            visible: false
          });
        }

        // send new list to subscribers
        this._messages.next(this._messages.getValue().push(...messages));

        // release the data payload from the response
        delete response.data;

        // set state received from Oswald
        this._state.next(response);

    }

    public messages() {
        return this._messages;
    }

    public state() {
        return this._state;
    }
}
