export interface Message {
  message?: string;
}

export interface OswaldMessageInterface extends Message {
  type?: string;
  elements?: any;
}

export class OswaldMessage implements OswaldMessageInterface {
  type?: string;
  message?: string;
  elements?: any;
  chatbot?: string;

  constructor(data?: OswaldMessageInterface) {
    Object.assign(this, data);
  }
}

export interface EmitMessageInterface extends Message {
    session?: string;
    data?: Array<any>;
}

export class EmitEvent {
  session?: string;
  type?: string;
  data?: any;
}

export class EmitMessage implements EmitMessageInterface {
    session?: string;
    message?: string;
    type?: string;
    data?: Array<any>;
    chatbot?: string;
    text?: string;

  constructor(data?: EmitMessageInterface) {
    Object.assign(this, data);
  }
}

export interface ChatMessageInterface {
    message?: Message;
    time?: Date;
    send: boolean;
    visible: boolean;
    chatbot?: string;
    processed?: string;
    environment?: string,
}

export class ChatMessage implements ChatMessageInterface {
    message?: any;
    time?: Date;
    send: boolean;
    visible: boolean;

  constructor(data?: ChatMessageInterface) {
    Object.assign(this, data);
  }
}
