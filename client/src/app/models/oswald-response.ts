import { OswaldMessage } from './message';

export interface OswaldResponseInterface {
  session: string;
  textDisabled?: boolean;
  placeholder?: string;
  data?: [OswaldMessage];
}

export class OswaldResponse implements OswaldResponseInterface {
  session: string;
  textDisabled?: boolean;
  placeholder?: string;
  data?: [OswaldMessage];

  constructor(data?: OswaldResponseInterface) {
    Object.assign(this, data);
  }
}
